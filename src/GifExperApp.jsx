import { useState } from "react";
import { AddCategory } from "./components/AddCategory";


export const GifExperApp = () => {
    const [categories, setCategories] = useState(
        ['One Punch','Dragon Ball']);

        const onAddCategory = ( newCategory ) => {

            if(categories.includes(newCategory)) return;
            // const newCategory = 'Honter x Honter';
            setCategories([...categories, newCategory] );
            // setCategories( cat => [...cat, 'Hunter x Hunter'] );
        }
    console.log(categories);

  return (
    <>
        <h1>GifExperApp</h1>

        <AddCategory 
        //setCategories={setCategories}
        onNewCategory={ (value)=> onAddCategory(value)}
        
         />

        


        <ol>
            {
            categories.map(category => {
                return<li key={category}>{category}</li>
                
            })
            }
            
        </ol>
    </>
  )
}
