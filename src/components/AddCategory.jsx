import { useState } from "react"


export const AddCategory = ({onNewCategory}) => {

const [inputValue, setInputValue] = useState('')

const agregarNombre = ({target}) => {
    setInputValue(target.value);
}

const onSubmitAdd = (event) => {
    event.preventDefault();
    //realizamos la validación con más de una letra
    if(inputValue.trim().length<=1)return;
    // setCategories(categories => [inputValue, ...categories] );
    onNewCategory( inputValue.trim() )
    setInputValue('');
}

  return (
    <form onSubmit={onSubmitAdd}>
      <input
      type="text"
      placeholder="Buscar Gifs"
      value={inputValue}
      onChange={agregarNombre}
      />
    </form>
  )
}
